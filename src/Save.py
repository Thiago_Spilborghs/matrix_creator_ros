#!/usr/bin/python 

import rospy
import time
from std_msgs.msg import String, Float32

class Client():
	def __init__(self):	
		self.pub = rospy.Publisher('/key',String,queue_size=1)
		self.execute()

	def execute(self):
		while True:
			try:
				x = raw_input()
				self.pub.publish(x)
				time.sleep(2)
				self.pub.publish('n')

			except KeyboardInterrupt:
				exit()


if __name__ == "__main__":
	rospy.init_node('Sender', anonymous=True)
	Client()
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
