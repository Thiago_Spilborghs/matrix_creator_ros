#!/usr/bin/python 

import socket
import rospy
from std_msgs.msg import String, Float32
import math

def to_teta(teta_monstra):
	return math.radians(teta_monstra)

class Client():
	def __init__(self):	
		self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.host = rospy.get_param('~HOST')
		self.helper = 0
		self.mean = 0.0
		self.port = rospy.get_param('~PORT')
		self.Listen = False
		self.save = None
		self.vec30 = []
		self.vec50 = []
		self.vec70 = []
		self.vec90 = []
		self.vec110 = []
		self.vec130 = []
		self.vec150 = []
		self.vec170 = []
		self.vec190 = []
		rospy.Subscriber('/key',String,self.callback)		
		self.pub = rospy.Publisher('/rotate',Float32,queue_size=1)
		#self.pub2 = rospy.Publisher('/Energy',Float32,queue_size=1)		
		self.serversocket.bind((self.host, self.port))
		self.serversocket.listen(10)
		self.conn, self.addr = self.serversocket.accept()
		while True:
			self.execute()
			if self.Listen == True:
				self.serversocket.bind((self.host, self.port))
				self.serversocket.listen(10)
				self.conn, self.addr = self.serversocket.accept()
				self.Listen = False

	def callback(self,data):
		dt = data.data
		self.save = dt

	def execute(self):
		try:
			print 'Socket listening'
			data = self.conn.recv(1024)
			if data:
				if data[-1] == 'v':
					data = data[:-1]
				x = to_teta(data)
				self.pub.publish(x)
			if self.save == 'x':
				print "saving to file"
				os.system('echo ' + repr(self.vec30)+ ' > /home/nailed-it-tm/30halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec50)+ ' > /home/nailed-it-tm/50halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec70)+ ' > /home/nailed-it-tm/70halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec90)+ ' > /home/nailed-it-tm/90halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec110)+ ' > /home/nailed-it-tm/110halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec130)+ ' > /home/nailed-it-tm/130halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec150)+ ' > /home/nailed-it-tm/150halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec170)+ ' > /home/nailed-it-tm/170halcreator_112_5.txt')
				os.system('echo ' + repr(self.vec190)+ ' > /home/nailed-it-tm/190halcreator_112_5.txt')
				self.save = 'n'
			if self.save == 'a':
				self.vec30.append(x)
				self.save = 'n'
			if self.save == 's':
				self.vec50.append(x)
				self.save = 'n'
			if self.save == 'd':
				self.vec70.append(x)
				self.save = 'n'
			if self.save == 'f':
				self.vec90.append(x)
				self.save = 'n'
			if self.save == 'g':
				self.vec110.append(x)
				self.save = 'n'
			if self.save == 'h':
				self.vec130.append(x)
				self.save = 'n'
			if self.save == 'j':
				self.vec150.append(x)
				self.save = 'n'
			if self.save == 'k':
				self.vec170.append(x)
				self.save = 'n'
			if self.save == 'l':
				self.vec190.append(x)
				self.save = 'n'
				  
		except socket.timeout as e:
			self.serversocket.close()
			self.Listen = True
			rospy.loginfo(e)
			return
		except socket.error as e:
			rospy.loginfo('Error on Server: %s'% e)
			self.Listen = True
			self.serversocket.close()
		except KeyboardInterrupt:
			self.serversocket.close()
			self.Listen = True
			exit()

			

if __name__ == "__main__":
	rospy.init_node('ClientMCreator', anonymous=True)
	Client()
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
