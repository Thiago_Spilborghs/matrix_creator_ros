#!/usr/bin/python 

import socket
import os
import rospy
from std_msgs.msg import String, Float32
import math


def update_angle(ang,x,y):
	if x >=0.0 and y>=0.0:
		ang = ang
	elif x>=0.0 and y<0.0:
		ang = ang
	elif x<0 and y <0:
		ang = ang - math.pi
	else:
		ang = ang + math.pi

	return ang

def to_teta(msg):
	a = msg.split()
	ctrl = 0
	x1 = 0.0
	y1 = 0.0
	for i in a:
		if i == '"x":':
			h = a[ctrl+1]
			x1 = float(h[:-1])
		if i == '"y":':
			h = a[ctrl+1]
			y1 = float(h[:-1])
			break
		ctrl = ctrl + 1

	if x1 == 0.0:
		m1 = (y1-0.0)/(x1-0.1)
	else:
		m1 = (y1-0.0)/(x1-0.0)	
	ang = math.atan(m1)
	ang = update_angle(ang,x1,y1)
	
	return ang

class Client():
	def __init__(self):	
		self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.host = rospy.get_param('~HOST')
		self.helper = 0
		self.mean = 0.0
		self.port = rospy.get_param('~PORT')
		self.Listen = False
		self.pub = rospy.Publisher('/rotate',Float32,queue_size=1)
		#self.pub2 = rospy.Publisher('/Energy',Float32,queue_size=1)		
		self.serversocket.bind((self.host, self.port))
		self.serversocket.listen(10)
		self.conn, self.addr = self.serversocket.accept()
		while True:
			self.execute()
			if self.Listen == True:
				self.serversocket.bind((self.host, self.port))
				self.serversocket.listen(10)
				self.conn, self.addr = self.serversocket.accept()
				self.Listen = False

	def execute(self):
		try:
			print 'Socket listening'
			data = self.conn.recv(1024)
			if data:
				x = to_teta(data)
				self.pub.publish(x)
				  
		except socket.timeout as e:
			self.serversocket.close()
			self.Listen = True
			rospy.loginfo(e)
			return
		except socket.error as e:
			rospy.loginfo('Error on Server: %s'% e)
			self.Listen = True
			self.serversocket.close()
		except KeyboardInterrupt:
			self.serversocket.close()
			self.Listen = True
			exit()

			

if __name__ == "__main__":
	rospy.init_node('ClientMCreator', anonymous=True)
	Client()
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
